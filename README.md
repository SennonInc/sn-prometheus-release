# Sennon Prometheus

Typescript file combiner for node scripts.
Used to put all separate files containing script classes into one file that
gets compiled into javascript. The javscript output is easier to uglify than
a directory of javascript code, and there is less repeated overhead due to
importing and exporting code across files.

Goals :
1. Keeps working code usable as is for easier debugging. No need to figure
  out which part of the uglified code is breaking when running the code as is.
2. Keeps the js outputs clean by making a single working typescript file before
  converting to javascript. Going from typescript to javascript involves
  creating a lot of overhead which is avoided by combining code at the
  typescript level instead of combining compiled js outputs.
3. Creates a consistent pattern for node scripts, whether they are actual node
  modules or just node scripts locally used. It seems like there is no standard
  for how to structure a node tool, so prometheus attempts to solve this issue
  by introducing a "main with a header and costum" classes pattern to the
  equation.
4. By having all shared libraries, constants, and other variables defined in
  the main_header file, it makes it easier to make sure nothing gets loaded
  twice if it is used in more than one tool class.
5. The main file should include the necessary args check to make the tool
  usable in the console, and it should return the main tool class as the
  exports so that other node scripts can use the tool internally.
6. The tool should create or export a main class which has defined
  functionality, as well as offer the necessary command lines options to use
  the tool directly from the console. All command lines options should be
  shortcuts to using the tool class directly in a node script.
7. Classes used to separate tool functionality should have the necessary
  interfaces defined in order to have default and custom behavior available
  as needed, without breaking class expectations.

A specific pattern is used to make scripts with this tool. All external imports
and global definitions should be exported from the main_header.ts file. Each
class necessary for the code to work should be defined in its own file. There
should be at least one main class which is initialized (running main.ts/output
directly) or exported (requiring the script in another script). The main.ts file
acts like a normal program's starting point and should checks the script's
user arguments if it has to run the tool directly. The main.ts file acts as a
console intermediary to requiring the tool in another script and accessing
its functionality that way.

Each main aspect of the tool's functionality and its variables needed to run
as expected should be handled in the main.ts file. Using the prometheus tool
as an example, we can see that the tool is broken down into five main files.
The Prometheus class (main class of the tool) has
