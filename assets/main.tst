#!/usr/bin/env node

import {
	{!mainClass}
} from './';

(function() {
	'use strict';

  let {!bin} = new {!mainClass}();
	
	if (module === require.main) {
    // Do stuff with {!bin}
	} else {
		module.exports = {!bin};
	}
})();
