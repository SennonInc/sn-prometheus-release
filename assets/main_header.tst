import * as fs	          from 'fs';
import * as tchalk        from 'chalk';
import * as commander     from 'commander';
import * as promptly      from 'promptly';

export const TEST = 'test';

export let
chalk       = tchalk,
command     = commander,
fileSystem  = fs,
prompt      = promptly;
